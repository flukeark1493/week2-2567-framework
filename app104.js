const calcArea = (radius) => {
  return pi * radius ** 2;
};
const pi = 3.14;
const area = calcArea(10, pi);
console.log("Area is : ", area);
